////GENERAL VARIABLE DECLARATION////
var humanPlayer = '';
var aiPlayer = '';
var playerWon = 0;
var play=false;
var playerMoves = 0;
var enemyMoves = 0;
var enemyCanMove = true;
const winLines = [
[0, 1, 2],
[3, 4, 5],
[6, 7, 8],
[0, 3, 6],
[1, 4, 7],
[2, 5, 8],
[0, 4, 8],
[6, 4 ,2]
];
var actualMoves = [];

var playCells = document.querySelectorAll('.play-cell');
for(var i = 0 ; i< playCells.length; i++){
	playCells[i].addEventListener('click', placeMark, false);
}
var playTiles = playCells.length;
/////////////////////////////////////////

function choosePlayer(typePlayer){
	humanPlayer = typePlayer;
	aiPlayer = (typePlayer== 'X') ? "O" : "X"; 
	console.log('Player choice: ' + humanPlayer + ' AI choice: ' + aiPlayer);
	play =true;
}
function placeMark(cell){
	let timeOut = 200;
	let square = document.getElementById(cell.target.id);
	if(play){
		if (square.innerHTML == ''){
			square.innerHTML =  humanPlayer;
			playerMoves++;
			playTiles--;
			checkWinner(humanPlayer);
			if(enemyCanMove){
				setTimeout(function(){ enemyMove(0); }, timeOut);

			} 
			updateBoardScore();
		}
	}else{
		alert('Please choose your mark first');	
	}
}
function checkEmptySquares(){
	var counter=[];
	for(var i = 0 ; i< playCells.length; i++){
		if(playCells[i].innerHTML == ''){
			counter.push(i);
		} 
	}
	return counter;
}
function updateBoardScore(){
	document.getElementById('p-move').innerHTML = playerMoves;
	document.getElementById('e-move').innerHTML = enemyMoves;
}
function checkWinner(player){
	for (var k = 0; k < winLines.length; k++){
		if(playCells[winLines[k][0]].innerHTML == player && playCells[winLines[k][1]].innerHTML == player && playCells[winLines[k][2]].innerHTML == player){
			gameWin(winLines[k][0], winLines[k][1], winLines[k][2], player);
			enemyMove(2);
			playerWon ++;

		}
	}
	checkTie();
}
function gameWin(a, b, c, player){
	for(let i =0 ; i< playCells.length; i++){
		playCells[i].removeEventListener('click', placeMark, false);
	}
	document.getElementById('square-'+a).style.backgroundColor = (player == humanPlayer)? '#57E25B' : '#EB1F1F';
	document.getElementById('square-'+b).style.backgroundColor = (player == humanPlayer)? '#57E25B' : '#EB1F1F';
	document.getElementById('square-'+c).style.backgroundColor = (player == humanPlayer)? '#57E25B' : '#EB1F1F';
	document.getElementById('overlay').style.display = 'block';
	document.getElementById('winText').innerHTML = 'Player ' + player + " has won the game!";
}
function enemyMove(enemyReact){
	let nextMove = bestSpot();
	nextMove.innerHTML = aiPlayer;
	playTiles--;
	enemyMoves++;
	checkWinner(aiPlayer);
	updateBoardScore();
	//console.log("mi siguiente movimiento sera: " + nextMove);
	//normalRoutine(0);
}
function bestSpot(){
	return minimax(playCells, aiPlayer).index;
	console.log( minimax(playCells, aiPlayer).index);
}
//*/*/*/*/////////////////////////////////////////*/*/*/*/*/*/*/*
function minimax(board, player){
	let freeSquares = checkEmptySquares();
	var moves = [];

	
	for (var i = 0; i< freeSquares.length; i++){
		var move= {};
		  //se observa cual
		  move.index = board[freeSquares[i]];
		  board[freeSquares[i]] = player;

		  if(player == aiPlayer){
		  	let result = seeTheFuture(board, humanPlayer);
		  	move.score = result.score;
		  	console.log("resultado Humano" + move.score);
		  }else{
		  	let result =  seeTheFuture(board, aiPlayer);
		  	move.score = result.score;
		  	console.log("resultado AI" + move.score);
		  }

		  board[freeSquares[i]] = move.index;
		  moves.push(move);
		}

	//Evalua el mejor movimiento
	var bestMove;

	if(player === aiPlayer){
		var bestScore = -10000;
		for (var i = 0; i< moves.length; i++ ){
			if(moves[i].score > bestScore){
				bestScore = moves[i].score;
				bestMove = i;
			}
		}
	}else {
		var bestScore = 10000;
		for (var i = 0; i< moves.length; i++ ){
			if(moves[i].score < bestScore){
				bestScore = moves[i].score;
				bestMove = i;
			}
		}
	}
	return moves[bestMove]; 
}

function seeTheFuture(board, player){
	var whoWon = null;
	for (var k = 0; k < winLines.length; k++){
		if(board[winLines[k][0]].innerHTML == player && board[winLines[k][1]].innerHTML == player && board[winLines[k][2]].innerHTML == player){
		whoWon = {index: k, player:player};	     
		
		}
	}
	if(whoWon != null){
	if(whoWon.player == humanPlayer){
				console.log("llegue a");
				return {score: -10};
			} else if(whoWon.player == aiPlayer){
				console.log("llegue b");
				return {score: 10};
			}
		}else{
			return {score: 0};
		}
}



/*////////////////////////////////////////////*/
function normalRoutine(enemyReact){
	for (let k = 0; k < winLines.length; k++){
		for(let i = 0; i < 3 ; i++){
			let square = document.getElementById('square-'+winLines[k][i]); 
			if(square.innerHTML == ''){
				enemyReact++;
				if(enemyReact == 1){
					playTiles--;
					square.innerHTML = aiPlayer;
					enemyMoves++;
					checkWinner(aiPlayer);
					updateBoardScore();
					console.log('cuadros libres: '+ checkEmptySquares());
				}
			}
		}
	}
}
function checkTie(){
	if(playTiles == 0 && playerWon == 0){
		for(let i = 0; i< playCells.length; i++){
			playCells[i].style.backgroundColor = '#F5D262';
		}
		document.getElementById('overlay').style.display = 'block';
		document.getElementById('winText').innerHTML = "It's a tie!";
	}
}
function restartGame(){
	document.getElementById('p-move').innerHTML = 0;
	document.getElementById('e-move').innerHTML = 0;
	for(let i = 0; i < playCells.length; i++){
		playCells[i].innerHTML = '';
		playCells[i].style.backgroundColor = 'transparent';
		playCells[i].addEventListener('click', placeMark, false);
	}
	humanPlayer = '';
	aiPlayer = '';
	playerMoves = 0;
	enemyMoves = 0;
	play = false;
	enemyCanMove = true;
	playTiles = playCells.length;
	document.getElementById('overlay').style.display = 'none';
} 


